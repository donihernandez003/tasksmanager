import firebase from 'firebase/app'

const firebaseConfig = {
  apiKey: 'AIzaSyDh4zikXV0Ffcv6kAkVHpnnrmuCPSCUSyo',
  authDomain: 'taskmanager-86fe8.firebaseapp.com',
  databaseURL: 'https://taskmanager-86fe8.firebaseio.com',
  projectId: 'taskmanager-86fe8',
  storageBucket: 'taskmanager-86fe8.appspot.com',
  messagingSenderId: '1056909950077',
  appId: '1:1056909950077:web:0dc04749a0287c1ace8514',
  measurementId: 'G-SVS8R3J34P'
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export default firebase
